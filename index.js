import express, { json } from "express";
import connectMongoDB from "./src/ConnectDB/connectMongoDB.js";
import { port } from "./src/Config/config.js";
import registerRouter from "./src/Register/registerRouter.js";
import cors from "cors";

let app = express();
app.use(cors());
app.use(json());

app.use("/registers", registerRouter);

// app.get("/verify", (req, res) => {
//   let token = req.query.token
//   res.redirect("/my-profile")
// })

connectMongoDB();

app.listen(port, () => {
  console.log(`Express Application is listening at ${port}`);
});
