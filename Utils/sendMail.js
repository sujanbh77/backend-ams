import nodemailer from "nodemailer";
import { emailHost, email, emailPassword, port } from "../src/Config/config.js";

let transporterInfo = {
  host: emailHost,
  port: 587,
  secure: false,
  auth: {
    user: email,
    pass: emailPassword,
  },
};

export let sendMail = async (mailInfo) => {
  try {
    let transporter = nodemailer.createTransport(transporterInfo);
    let info = await transporter.sendMail(mailInfo);
  } catch (error) {
    console.log("Error has occurred", error.message);
    throw error;
  }
};

  // try {
  //   await sendMail({
  //     from: '"AMS Bot" <AMS.PROJECT.DEERWALK2023@gmail.com>',
  //     to: req.body.email,
  //     subject: "Register Verification",
  //     html: `<h1>Your Account has been created</h1>`,
  //   });
  //   console.log("Email sent successfully");
  // } catch (error) {
  //   console.log("Error has occurred", error.message);
  //   throw error;
  // }

