import expressAsyncHandler from "express-async-handler";
import jwt from "jsonwebtoken";
import { v4 as uuidv4 } from "uuid";

export let generateToken = expressAsyncHandler(
  async (info, secretKey, expiryInfo) => {
    let token = await jwt.sign(info, secretKey, expiryInfo);
    return token;
  }
);

// export let userToken = uuidv4()

// export let verificationLink = `http://localhost:7000/verify?token=${userToken}`

export let verifyToken = expressAsyncHandler(async (token, secretKey) => {
  let info = await jwt.verify(token, secretKey);
  return info;
});
