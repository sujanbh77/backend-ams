import mongoose from "mongoose";
import { dbUrl } from "../Config/config.js";

let connectMongoDB = () => {
  mongoose.connect(dbUrl);
};

export default connectMongoDB;
