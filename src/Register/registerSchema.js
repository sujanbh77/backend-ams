import { Schema } from "mongoose";

let registerSchema = Schema({
  fullName: {
    type: String,
    required: [true, "Name field is required"],
    minLength: [4, "Name field must be at least 4 character"],
    maxLength: [20, "Name field must be at most 20 character"],
  },

  password: {
    type: String,
    required: [true, "Password field is required"],
  },

  email: {
    type: String,
    required: [true, "Email field is required"],

    validate: (value) => {
      let isValid = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(
        value
      );

      if (isValid === true) {
      } else {
        let error = new Error("Email is not valid");
        throw error;
      }
    },
  },

  role: {
    type: String,
    required: [true, "Role is required"],
  },

  contact: {
    type: Number,
    required: [true, "Contact is required"],

    validate: (value) => {
      let contactString = String(value);
      if (contactString.length != 10) {
        let error = new Error("Phone Number must be of 10 character");
        throw error;
      }
    },
  },
});

export default registerSchema;
