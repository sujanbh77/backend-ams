import { Router } from "express";
import {
  createRegister,
  deleteSpecificRegister,
  getMyProfile,
  loginRegister,
  readRegister,
  readSpecificRegister,
  updateSpecificRegister,
} from "./registerController.js";
import isAuthenticated from "../Middleware/isAuthenticated.js";
import { sendMail } from "../../Utils/sendMail.js";

let registerRouter = Router();

registerRouter.route("/").post(createRegister, sendMail).get(readRegister);

registerRouter.route("/login").post(loginRegister);

registerRouter.route("/my-profile").get(isAuthenticated, getMyProfile);

// registerRouter
//   .route("/my-profile-update")
//   .patch(isAuthenticated, updateMyProfile);

registerRouter
  .route("/:id")
  .get(readSpecificRegister)
  .patch(updateSpecificRegister)
  .delete(deleteSpecificRegister);

//   .delete(
//     isAuthenticated,
//     isAuthorization(["admin", "superAdmin"]),
//     deleteSpecificRegister
//   );

export default registerRouter;
