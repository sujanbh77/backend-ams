import expressAsyncHandler from "express-async-handler";
import successResponse from "../Helper/successResponse.js";
import { Register } from "../Model/model.js";
import { comparePassword, hashPassword } from "../../Utils/hashing.js";
import { secretKey } from "../Config/config.js";
import { generateToken } from "../../Utils/token.js";
import { sendMail } from "../../Utils/sendMail.js";

export let createRegister = expressAsyncHandler(async (req, res, next) => {
  let data = req.body;
  data.password = await hashPassword(data.password);
  let result = await Register.create(data);

  try {
    await sendMail({
      from: '"AMS Bot" <AMS.PROJECT.DEERWALK2023@gmail.com>',
      to: req.body.email,
      subject: "Register Verification",
      text: "Your account has been created successfully. Click the link below to login:",
      html: `<p>Your Account has been created successfully. Click the link below to login:</p><a href="http://localhost:7000/registers/my-profile">Verification Link</a>`,
    });
    console.log("Email sent successfully");
  } catch (error) {
    console.log("Error has occurred", error.message);
    throw error;
  }

  successResponse(res, 201, "Register created successfully", result);
});

export let loginRegister = expressAsyncHandler(async (req, res, next) => {
  let email = req.body.email;
  let password = req.body.password;
  // if password match generate token and send to db

  let user = await Register.findOne({ email: email });
  let dbPassword = user.password;

  let isPasswordMatch = await comparePassword(password, dbPassword);

  if (isPasswordMatch) {
    let info = {
      id: user._id,
      role: user.role,
    };

    let expiryInfo = {
      expiresIn: "365d",
    };

    let token = await generateToken(info, secretKey, expiryInfo);
    successResponse(res, 200, "User logged in successfully", token);
  } else {
    let error = new Error("Email or Password doesn't match");
    throw error;
  }
});

export let getMyProfile = expressAsyncHandler(async (req, res, next) => {
  let id = req.info.id;
  let result = await Register.findById(id);
  successResponse(res, 200, "My profile read successfully", result);
});

// export let updateMyProfile = expressAsyncHandler(async (req, res, next) => {
//     let id = req.info.id;
//     let data = req.body;
//     delete data.email;
//     delete data.password;
//     let result = await Register.findByIdAndUpdate(id, data);
//     successResponse(res, 201, "Updated My profile detail successfully", result);
// });

export let readRegister = expressAsyncHandler(async (req, res, next) => {
  let result = await Register.find({});

  successResponse(res, 200, "Read Register successfully", result);
});

export let readSpecificRegister = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let result = await Register.findById(id);
    successResponse(res, 200, "Read Register detail successfully", result);
  }
);

export let updateSpecificRegister = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let data = req.body;
    let result = await Register.findByIdAndUpdate(id, data);
    successResponse(res, 201, "Updated Register detail successfully", result);
  }
);

export let deleteSpecificRegister = expressAsyncHandler(
  async (req, res, next) => {
    let id = req.params.id;
    let result = await Register.findByIdAndDelete(id);
    successResponse(res, 200, "Deleted Register successfully", result);
  }
);
