import expressAsyncHandler from "express-async-handler";
import { secretKey } from "../Config/config.js";
import { verifyToken } from "../../Utils/token.js";
// import { secretKey } from "../Config/config.js";
// import { verifyToken } from "../Token/token.js";

let isAuthenticated = expressAsyncHandler(async (req, res, next) => {
  let bearerToken = req.headers.authorization;
  let arrBearerToken = bearerToken.split(" ");
  let token = arrBearerToken[1];

  let info = await verifyToken(token, secretKey);
  req.info = info;
  next();
});

export default isAuthenticated;
